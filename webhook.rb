#!/usr/bin/ruby

# Copyright (C) 2018, Joerg Jaspert <joerg@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Load required libraries
# Eventmachine for deferred execution
require 'eventmachine'
# We use sinatra here
require 'sinatra/base'
require "sinatra"
# Allow to read app settings from yaml file
require "sinatra/config_file"
# And the webserver framework for eventmachine
require 'thin'
# We define our logging stuff
require 'logger'

# Beware, a global variable.
# Store our main directory so we can use it later to easily find things.
$maindir=File.expand_path(File.dirname(__FILE__))

# Load our config file
config_file File.join($maindir, 'config.yml')

# Setup our application environment
def run()
  # Start he reactor
  EM.run do
    dispatch = Rack::Builder.app do
      map '/' do
        run SalsaWebHook.new
      end
    end

    # Start the web server.
    Rack::Server.start({
                         app:    dispatch,
                         server: settings.servertype,
                         Host:   settings.bind,
                         Port:   settings.port,
                         signals: false,
                       })
  end # EM.run
end # def run

# And here goes our actual webapp (hook) logic
class SalsaWebHook < Sinatra::Application

  # Rack wants to call write on the logger, define it
  ::Logger.class_eval { alias :write :'<<' }

  if settings.logging
    # Define our logfiles
    # Apache access.log style
    access_log = ::Logger.new(File.join($maindir, "access.log"), "daily")
    access_log.level = settings.log_level
    # Error log for code errors
    error_log = ::File.new(::File.join($maindir,'error.log'),"a+")
    error_log.sync = true
  end

  # Set some defaults, run once at beginning
  configure do
    if settings.logging
      # Use our logger
      use ::Rack::CommonLogger, access_log
    end

    # queue request for background thread
    set :threaded, true

    # Do not show exceptions
    set :show_exceptions, false
  end

  # Some defaults.
  before do
    # Return plain text. Gitlab talks with us, gitlab doesn't care
    # what we spit out. Make it plaintext...
    content_type :txt
    if settings.logging
     # To get errors, ensure it goes to our error_logger
     env["rack.errors"] =  error_log
    end
  end

  def scan_bugs(text)
    text.scan(/Closes:\s+(?:Bug)?#(?:(\d{4,8})\b)(?:,?\s*(?:Bug)?#(?:(\d{4,8})\b))*/i)
  end

  # The / endpoint doesn't do anything special, just tells users they
  # are wrong here. Everything that actually does something useful is in
  # a plugin in the plugins dir.
  post '/' do
    # A human trying to talk to us - or someone misconfiguring their hook
    <<-EOERROR
You reached a webhook. That means either you try this as a user in a browser,
which does not do any good, or you misconfigured your webhook settings.
EOERROR
  end
  # And for users with a webbrowser, we support get.
  get '/' do
    raise "foo"
    # A human trying to talk to us - or someone misconfiguring their hook
    <<-EOERROR
You reached a webhook. That means either you try this as a user in a browser,
which does not do any good, or you misconfigured your webhook settings.
EOERROR
  end

  # Now load each plugin, which should declare their route and code
  settings.plugins.each do |plugin|
    require_relative "plugins/#{plugin}"
  end
end # class SinatraWebHook

# And last line - run this whole thingie
run
