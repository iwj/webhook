# GitLab webhooks for Salsa

This project contains the GitLab [webhooks](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html)
deployed at https://webhook.salsa.debian.org.

The webhooks available are:

* https://webhook.salsa.debian.org/close/SOURCENAME <br/>
  This hook detects bugs closed in commit messages and closes them in the Debian Bug Tracking System.

* https://webhook.salsa.debian.org/tagpending/SOURCENAME <br/>
  This hook detects bugs closed in commit messages and tag them as pending in the Debian Bug Tracking System.

SOURCENAME is the name of the source package.

## Development

To run the webhooks locally:

    apt-get install ruby ruby-mail ruby-sinatra ruby-sinatra-contrib ruby-rack ruby-soap4r thin
    
    ruby webhook.rb -e development
